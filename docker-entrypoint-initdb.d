#!/usr/bin/env bash

echo 'Creating application user and db'

mongo orders \
        --host mongo \
        --port 27017 \
        -u user \
        -p password \
        --authenticationDatabase admin \
        --eval ";"db.createUser({user: 'app_user', pwd: 'app_pass', roles:[{role:'dbOwner', db: 'orders'}], passwordDigestor : "server"})