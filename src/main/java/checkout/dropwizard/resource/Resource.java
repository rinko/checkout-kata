package checkout.dropwizard.resource;

import checkout.dropwizard.checkout.Checkout;
import checkout.dropwizard.order.Item;
import checkout.dropwizard.order.Order;
import checkout.dropwizard.order.OrderDao;
import checkout.dropwizard.price.Price;
import checkout.dropwizard.price.PriceHandler;
import com.google.gson.Gson;
import com.google.inject.Inject;
import io.dropwizard.jersey.PATCH;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.ArrayList;

@Path("/checkout")
@Produces(MediaType.APPLICATION_JSON)
public class Resource {

    private OrderDao orderDao;
    private Gson gson;
    private PriceHandler priceHandler;
    private Checkout checkout;

    @Inject
    public Resource(OrderDao orderDao, Gson gson, PriceHandler priceHandler, Checkout checkout) {
        this.orderDao = orderDao;
        this.gson = gson;
        this.priceHandler = priceHandler;
        this.checkout = checkout;
    }

    @POST
    @Path("order/{orderId}")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response createOrder(@PathParam("orderId") int orderId) {
        Response.ResponseBuilder response;
        response = Response.ok();
        Order order = new Order();
        order.setItems(new ArrayList<Item>());
        order.setOrderId(orderId);
        order.setOrderTotal(BigDecimal.valueOf(0));
        try {
            orderDao.save(order);
            response.entity("{\"message\": \"Order created\"}");
        } catch (Exception e) {
            response.entity("{\"message\": \"Order already exists\"}");
        }
        return response.build();
    }

    @GET
    @Path("order/{orderId}")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getOrder(@PathParam("orderId") int orderId) {
        Order order = orderDao.getOne(orderId);
        Response.ResponseBuilder response;
        if (order == null) {
            response = Response.status(404);
            response.entity("{\"message\": \"Order not found\"}");

        } else {
            response = Response.ok();
            response.entity(gson.toJson(order));
        }
        return response.build();
    }

    @POST
    @Path("price")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response insertPrice(Price price){
        Response.ResponseBuilder response;
        if (priceHandler.insertPrice(price)) {
            response = Response.ok();
            response.entity("{\"message\": \"Price Inserted\"}");
        } else {
            response = Response.serverError();
            response.entity("{\"message\": \"Duplicate item number\"}");
        }
        return response.build();
    }

    @PATCH
    @Path("price")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response updatePrice(Price price){
        Response.ResponseBuilder response;
        if (priceHandler.update(price)) {
            response = Response.ok();
            response.entity("{\"message\": \"Price Updated\"}");
        } else {
            response = Response.serverError();
            response.entity("{\"message\": \"Error Updating Price\"}");
        }
        return response.build();
    }

    @GET
    @Path("price/{itemNumber}")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getItem(@PathParam("itemNumber") int itemNumber){
        Response.ResponseBuilder response;
        try {
            Price price = priceHandler.getPrice(itemNumber);
            if (price == null) {
                response = Response.status(404);
                response.entity("{\"message\": \"Price not found\"}");
            } else {
                response = Response.ok();
                response.entity(gson.toJson(price));
            }
        } catch (Exception e) {
            response = Response.serverError();
            response.entity("{\"message\": \"Unable to get price\"}");
        }
        return response.build();
    }

    @POST
    @Path("order/{orderId}/item/{itemNumber}/quantity/{quantity}")
        public Response postItemToOrder(@PathParam("orderId")int orderId, @PathParam("itemNumber") int itemNumber, @PathParam("quantity") int quantity) {
        Response.ResponseBuilder response;
        try {
            Order order = checkout.addItemToOrder(orderId, itemNumber, quantity);
//            orderDao.update(order);
            response = Response.ok();
            response.entity(gson.toJson(order));
        } catch (Exception e) {
            response = Response.serverError();
            response.entity("{\"message\": \"Unable to add to Order\"}");
        }
        return response.build();
    }


}
