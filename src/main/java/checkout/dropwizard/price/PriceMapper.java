package checkout.dropwizard.price;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PriceMapper implements ResultSetMapper<Price> {

    public Price map(int index, ResultSet resultSet, StatementContext statementContext) throws SQLException {

        return new Price(
                resultSet.getInt("item_number"),
                resultSet.getBigDecimal("price"),
                resultSet.getInt("promo_units"),
                resultSet.getBigDecimal("promo_price")
        );
    }

}
