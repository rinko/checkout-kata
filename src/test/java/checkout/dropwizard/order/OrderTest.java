package checkout.dropwizard.order;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;

public class OrderTest {

    private Order order;
    private Item item;

    @Before
    public void before() {
        order = new Order();
        order.setOrderId(1);
        order.setOrderTotal(BigDecimal.valueOf(1));

        List<Item> items = new ArrayList<>();
        item = new Item(1, BigDecimal.valueOf(1), 1);
        items.add(item);
        order.setItems(items);

    }

    @Test
    public void testOrderHasItemReturnsTrue() {
        assertThat(order.hasItems(item.getItemNumber()), Matchers.is(true));
    }

    @Test
    public void testOrderHasItemReturnsFalseWhenNoItem() {
        assertThat(order.hasItems(2), Matchers.is(false));
    }

    @Test
    public void testOrderGetReturnsItem() {
        assertThat(order.getItems(1), Matchers.is(item));
    }


    @Test
    public void testOrderGetReturnsNull() {
        assertThat(order.getItems(2), Matchers.is(Matchers.nullValue()));
    }

    @Test
    public void testOrderReplaceItemUpdatesAnItem() {
        item.setQuantity(2);
        order.replaceItem(item);
        assertThat(order.getItems(1).getQuantity(), Matchers.is(2));
    }

}
