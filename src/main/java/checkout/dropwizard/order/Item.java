package checkout.dropwizard.order;

import java.math.BigDecimal;

public class Item {

    public Item() {

    }

    public Item(int itemNumber, BigDecimal totalPrice, int quantity) {
        this.itemNumber = itemNumber;
        this.totalPrice = totalPrice;
        this.quantity = quantity;
    }

    private int itemNumber;
    private BigDecimal totalPrice;
    private int quantity;

    public int getItemNumber() {
        return itemNumber;
    }

    public void setItemNumber(int itemNumber) {
        this.itemNumber = itemNumber;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
