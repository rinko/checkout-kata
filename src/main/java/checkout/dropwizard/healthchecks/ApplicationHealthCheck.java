package checkout.dropwizard.healthchecks;

import com.codahale.metrics.health.HealthCheck;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class ApplicationHealthCheck extends HealthCheck {

    @Inject
    public ApplicationHealthCheck() {

    }

    @Override
    protected Result check() throws Exception {
        return Result.healthy();
    }
}
