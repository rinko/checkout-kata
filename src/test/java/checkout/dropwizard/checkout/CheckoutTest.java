package checkout.dropwizard.checkout;

import checkout.dropwizard.order.Item;
import checkout.dropwizard.order.Order;
import checkout.dropwizard.order.OrderDao;
import checkout.dropwizard.price.Price;
import checkout.dropwizard.price.PriceHandler;
import com.mongodb.MongoException;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;

public class CheckoutTest {

    @Mock
    OrderDao orderDaoMock;
    @Mock
    PriceHandler priceHandlerMock;

    private Checkout checkout;

    private Order order;
    private Price price;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
        checkout = new Checkout(orderDaoMock, priceHandlerMock);

        order = new Order();
        order.setOrderId(1);
        order.setOrderTotal(BigDecimal.valueOf(1));

        List<Item> items = new ArrayList<>();
        Item item = new Item(1, BigDecimal.valueOf(1), 1);
        items.add(item);
        order.setItems(items);

        price = new Price(1, BigDecimal.valueOf(2), 2, BigDecimal.valueOf(1));
    }

    @Test
    public void testAddingItemToOrderWhenItemExists() throws Exception {
        when(orderDaoMock.getOne(1)).thenReturn(order);
        when(priceHandlerMock.getPrice(1)).thenReturn(price);

        Order orderReturn = checkout.addItemToOrder(1, 1, 1);
        assertThat(orderReturn.getOrderTotal(), Matchers.is(BigDecimal.valueOf(2)));

        verify(orderDaoMock, times(1)).update(orderReturn);
    }

    @Test
    public void testAddingItemToOrderWhenItemDoesNotExist() throws Exception {
        when(orderDaoMock.getOne(1)).thenReturn(order);
        when(priceHandlerMock.getPrice(2)).thenReturn(price);

        Order orderReturn = checkout.addItemToOrder(1, 2, 1);
        assertThat(orderReturn.getOrderTotal(), Matchers.is(BigDecimal.valueOf(3)));

        verify(orderDaoMock, times(1)).update(orderReturn);
    }

    @Test(expected = Exception.class)
    public void testAddToOrderThrowsException() throws Exception {
        when(orderDaoMock.getOne(1)).thenReturn(order);
        doThrow(new Exception("Test")).when(priceHandlerMock).getPrice(1);
        checkout.addItemToOrder(1, 1, 1);

    }



}
