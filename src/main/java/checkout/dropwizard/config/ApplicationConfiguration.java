package checkout.dropwizard.config;


import checkout.dropwizard.database.mongo.configuration.MongoDBConnection;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class ApplicationConfiguration extends Configuration {

    @Valid
    @NotNull
    @JsonProperty("database")
    private DataSourceFactory database = new DataSourceFactory();

    public DataSourceFactory getDatabase() {
        return database;
    }

    private MongoDBConnection mongoDBConnection;

    public MongoDBConnection getMongoDBConnection() {
        return mongoDBConnection;
    }

}
