package checkout.dropwizard.price;

import com.google.inject.Inject;
import org.postgresql.util.PSQLException;

public class PriceHandler {

    private PriceDao priceDao;

    @Inject
    public PriceHandler(PriceDao priceDao) {
        this.priceDao = priceDao;
    }

    public boolean insertPrice(Price price) {
        try {
            if (priceDao.insert(price) == 1) {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }

    public Price getPrice(int itemNumber) throws Exception {
        try {
            return priceDao.getPrice(itemNumber);
        } catch (PSQLException e) {
            throw new Exception("To ");
        }

    }

    public boolean update(Price price) {
        try {
            if (priceDao.update(price) == 1) {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }
}
