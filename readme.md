Docker-Compose is required to run this project: https://docs.docker.com/compose/gettingstarted/

**Running the app**  

    cd checkout
    ./gradlew testAndBuild
    docker-compose up --force-recreate db mongo checkout  

The gradle task runs all unit tests, builds jar and creates a docker image
docker compose starts 3 containers (1 service, 2 dbs)
  
**Technologies used**  
1 x MongoDB for storing orders  
1 x Postgres for storing prices  
DB schema: src/main/resources/db/migration/V1_1__price.sql  
Docker compose for db instances and service  
  
Didn't have time for the swagger, hope you don't mind using the following!  
Base URL (http://localhost:8080/checkout/)  
Endpoints:

*'Creates an order, given the id'*  
POST  /order/{orderId}  
  
*'Gets an order, given the id'*  
GET  /order/{orderId}  
  
*'Posts a price in the database'*  
POST  /price  
Example json body  

> { "itemNumber":1, "price":2, "promoUnits":2, "promoPrice":1 }

*'Updates a price in the database'*  
PATCH  /price  
Example json body  

> { "itemNumber":1, "price":2, "promoUnits":2, "promoPrice":1 }


*'Gets a price, given the item number'*  
GET  /price/itemNumber  
  
*'Posts an item and the quantity to an order, returning the updated order'*  
POST  /order/{orderId}/item/{itemNumber}/quantity/{quantity}
