package checkout.dropwizard;

import checkout.dropwizard.config.ApplicationConfiguration;
import checkout.dropwizard.database.postgres.FlywayDB;
import checkout.dropwizard.module.ServerModule;
import com.hubspot.dropwizard.guicier.GuiceBundle;
import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class App extends Application<ApplicationConfiguration> {

    public static void main(String[] args) throws Exception {
        new App().run(args);
    }

    @Override
    public void initialize(Bootstrap<ApplicationConfiguration> bootstrap) {
        GuiceBundle<ApplicationConfiguration> guiceBundle = GuiceBundle.defaultBuilder(ApplicationConfiguration.class)
                .modules(new ServerModule())
                .build();
        bootstrap.addBundle(guiceBundle);
    }

    @Override
    public void run(ApplicationConfiguration configuration, Environment environment) throws Exception {
        DataSourceFactory dataSourceFactory = configuration.getDatabase();
        new FlywayDB().checkForSchemaUpdates(dataSourceFactory, configuration);
    }

}