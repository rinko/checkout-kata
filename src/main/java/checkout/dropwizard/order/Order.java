package checkout.dropwizard.order;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

public class Order {

    @NotNull
    private int orderId;
    private List<Item> items;
    private BigDecimal orderTotal;

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public BigDecimal getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(BigDecimal orderTotal) {
        this.orderTotal = orderTotal;
    }

    public boolean hasItems(int itemNumber) {
        for (Item i: items) {
            if (i.getItemNumber() == itemNumber)
                return true;
        }
        return false;
    }

    public Item getItems(int itemNumber) {
        for (Item i: items) {
            if (i.getItemNumber() == itemNumber)
                return i;
        }
        return null;
    }

    public void replaceItem(Item item) {
        int index;
        for (Item i: items) {
            if (i.getItemNumber() == item.getItemNumber()) {
                index = items.indexOf(i);
                items.set(index, item);
            }

        }
    }



}
