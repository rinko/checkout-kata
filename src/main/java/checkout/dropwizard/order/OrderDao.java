package checkout.dropwizard.order;

import com.mongodb.client.MongoCollection;

import java.util.Optional;

import static com.mongodb.client.model.Filters.eq;

public class OrderDao {

    final MongoCollection<Order> orderCollection;

    public OrderDao(final MongoCollection<Order> orderCollection) {
        this.orderCollection = orderCollection;
    }

    public void save(final Order order) throws Exception {
        if (orderCollection.countDocuments(eq("orderId", order.getOrderId())) > 0) {
            throw new Exception("Order already exists");
        }
        orderCollection.insertOne(order);
    }

    public void update(Order order) {
        orderCollection.findOneAndReplace(eq("orderId", order.getOrderId()), order);
    }

    public Order getOne(int orderId) {
        Optional<Order> orderFind = Optional.ofNullable(orderCollection.find(eq("orderId", orderId)).first());
        return orderFind.isPresent() ? orderFind.get() : null;
    }




}
