package checkout.dropwizard.database.postgres;

import checkout.dropwizard.config.ApplicationConfiguration;
import io.dropwizard.db.DataSourceFactory;
import org.flywaydb.core.Flyway;

public class FlywayDB {

    private String sqlLocation = "/db/migration";

    public void checkForSchemaUpdates(DataSourceFactory dataSourceFactory, ApplicationConfiguration configuration) {
        Flyway.configure()
                .locations(getSqlLocation())
                .dataSource(dataSourceFactory.getUrl(), dataSourceFactory.getUser(), dataSourceFactory.getPassword())
                .load()
                .migrate();
    }

    public String getSqlLocation() {
        return sqlLocation;
    }
}
