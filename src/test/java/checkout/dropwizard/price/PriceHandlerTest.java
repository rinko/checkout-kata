package checkout.dropwizard.price;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.postgresql.util.PSQLException;
import org.postgresql.util.ServerErrorMessage;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

public class PriceHandlerTest {

    @Mock
    PriceDao priceDaoMock;

    PriceHandler priceHandler;
    Price price;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
        priceHandler = new PriceHandler(priceDaoMock);
        price = new Price(1, BigDecimal.valueOf(1), 1, BigDecimal.valueOf(1));
    }

    @Test
    public void testPriceHandlerInsertsPrice() {
        when(priceDaoMock.insert(price)).thenReturn(1);
        assertThat(priceHandler.insertPrice(price), Matchers.is(true));
    }

    @Test
    public void testPriceHandlerCannotInsertPrice() {
        when(priceDaoMock.insert(price)).thenReturn(0);
        assertThat(priceHandler.insertPrice(price), Matchers.is(false));
    }

    @Test
    public void testPriceHandlerGetsAPrice() throws Exception {
        when(priceDaoMock.getPrice(1)).thenReturn(price);
        assertThat(priceHandler.getPrice(1), Matchers.is(price));
    }

    @Test(expected = Exception.class)
    public void testPriceHandlerGetPriceThrowsException() throws Exception {
        doThrow(new PSQLException(new ServerErrorMessage("tes"))).when(priceDaoMock.getPrice(1));
        priceHandler.getPrice(1);
    }

    @Test
    public void testPriceHandlerUpdatesAPrice() throws Exception {
        when(priceDaoMock.update(price)).thenReturn(1);
        assertThat(priceHandler.update(price), Matchers.is(true));
    }

    @Test
    public void testPriceHandlerIsUnableToInsert() throws Exception {
        when(priceDaoMock.update(price)).thenReturn(0);
        assertThat(priceHandler.update(price), Matchers.is(false));
    }

}
