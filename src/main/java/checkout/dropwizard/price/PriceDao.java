package checkout.dropwizard.price;

import org.postgresql.util.PSQLException;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;

import java.util.List;


public interface PriceDao {

    @Mapper(PriceMapper.class)
    @SqlQuery("SELECT * FROM PRICE")
    List<Price> getAll() throws PSQLException;;

    @Mapper(PriceMapper.class)
    @SqlQuery("SELECT * FROM PRICE WHERE item_number = :itemNumber")
    Price getPrice(@Bind("itemNumber") int itemNumber) throws PSQLException;;

    @SqlUpdate("INSERT INTO price(item_number, price, promo_units, promo_price) VALUES (:p.itemNumber, :p.price, :p.promoUnits, :p.promoPrice)")
    int insert(@BindBean("p") Price price);

    @SqlUpdate("UPDATE price SET price = :p.price, promo_units = :p.promoUnits, promo_price = :p.promoPrice WHERE item_number = :p.itemNumber")
    int update(@BindBean("p") Price price);

}
