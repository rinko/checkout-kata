package checkout.dropwizard.price;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;


public class Price {
    public Price() {

    }

    public Price(int itemNumber, BigDecimal price, int promoUnits, BigDecimal promoPrice) {
        this.itemNumber = itemNumber;
        this.price = price;
        this.promoUnits = promoUnits;
        this.promoPrice = promoPrice;
    }
    @NotNull
    private int itemNumber;
    @NotNull
    private BigDecimal price;
    @NotNull
    private int promoUnits;
    @NotNull
    private BigDecimal promoPrice;

    public int getItemNumber() {
        return itemNumber;
    }

    public void setItemNumber(int itemNumber) {
        this.itemNumber = itemNumber;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getPromoUnits() {
        return promoUnits;
    }

    public void setPromoUnits(int promoUnits) {
        this.promoUnits = promoUnits;
    }

    public BigDecimal getPromoPrice() {
        return promoPrice;
    }

    public void setPromoPrice(BigDecimal promoPrice) {
        this.promoPrice = promoPrice;
    }
}
