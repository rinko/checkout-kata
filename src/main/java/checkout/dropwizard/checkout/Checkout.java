package checkout.dropwizard.checkout;

import checkout.dropwizard.order.Item;
import checkout.dropwizard.order.Order;
import checkout.dropwizard.order.OrderDao;
import checkout.dropwizard.price.Price;
import checkout.dropwizard.price.PriceHandler;
import com.google.inject.Inject;

import java.math.BigDecimal;

public class Checkout {

    OrderDao orderDao;
    PriceHandler priceHandler;

    @Inject
    public Checkout(OrderDao orderDao, PriceHandler priceHandler) {
        this.orderDao = orderDao;
        this.priceHandler = priceHandler;
    }

    public Order addItemToOrder(int orderId, int itemNumber, int quantity) throws Exception {
        Order order = orderDao.getOne(orderId);
        Price price = priceHandler.getPrice(itemNumber);

        if (order.hasItems(itemNumber)) {
            Item item = order.getItems(itemNumber);
            item.setTotalPrice(calculateTotalItemPrice(price, quantity + item.getQuantity()));
            item.setQuantity(item.getQuantity() + quantity);
            order.replaceItem(item);
            order.setOrderTotal(calculateTotalOrderPrice(order));
            orderDao.update(order);
        } else {
            order.getItems().add(new Item(itemNumber, calculateTotalItemPrice(price, quantity), quantity));
            order.setOrderTotal(calculateTotalOrderPrice(order));
            orderDao.update(order);
        }
        return order;
    }

    private BigDecimal calculateTotalItemPrice(Price price, int quantity) {
        int remainder = Math.floorMod(quantity, price.getPromoUnits());
        int deals = quantity - remainder;

        return BigDecimal.valueOf(deals).multiply(price.getPromoPrice()).add(BigDecimal.valueOf(remainder).multiply(price.getPrice()));
    }

    private BigDecimal calculateTotalOrderPrice(Order order) {
        BigDecimal total = BigDecimal.valueOf(0);
        for (Item i: order.getItems()) {
            total = total.add(i.getTotalPrice());
        }
        return total;
    }

}
