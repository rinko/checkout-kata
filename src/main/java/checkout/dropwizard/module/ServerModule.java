package checkout.dropwizard.module;

import checkout.dropwizard.checkout.Checkout;
import checkout.dropwizard.config.ApplicationConfiguration;
import checkout.dropwizard.healthchecks.ApplicationHealthCheck;
import checkout.dropwizard.order.Order;
import checkout.dropwizard.order.OrderDao;
import checkout.dropwizard.price.PriceDao;
import checkout.dropwizard.price.PriceHandler;
import checkout.dropwizard.resource.Resource;
import com.google.gson.Gson;
import com.google.inject.Binder;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.hubspot.dropwizard.guicier.DropwizardAwareModule;
import com.mongodb.*;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.setup.Environment;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.skife.jdbi.v2.DBI;
import java.util.ArrayList;
import java.util.List;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class ServerModule extends DropwizardAwareModule<ApplicationConfiguration> {

    @Override
    public void configure(Binder binder) {
        binder.bind(ApplicationHealthCheck.class);
        binder.bind(Resource.class);
        binder.bind(Gson.class).toInstance(new Gson());
        binder.bind(Checkout.class);
        binder.bind(PriceHandler.class);
    }

    @Provides
    @Singleton
    public DBI provideDBI(Environment environment, ApplicationConfiguration configuration) {
        DataSourceFactory dataSourceFactory = configuration.getDatabase();
        DBIFactory factory = new DBIFactory();
        return factory.build(environment, dataSourceFactory, "postgres");
    }

    @Provides
    public PriceDao providePriceDao(DBI dbi) {
        return dbi.onDemand(PriceDao.class);
    }

    @Singleton
    @Provides
    final MongoClient mongoClient(ApplicationConfiguration applicationConfiguration) {
        MongoCredential mongoCredential = MongoCredential.createCredential(
                applicationConfiguration.getMongoDBConnection().getUsername(),
                applicationConfiguration.getMongoDBConnection().getDatabase(),
                applicationConfiguration.getMongoDBConnection().getPassword().toCharArray());

        List<ServerAddress> seeds = new ArrayList<ServerAddress>();
        seeds.add( new ServerAddress( applicationConfiguration.getMongoDBConnection().getHost()));

        MongoClientOptions options = MongoClientOptions.builder()
                .writeConcern(WriteConcern.JOURNALED)
                .build();

        return new MongoClient(seeds, mongoCredential, options);
    }

    @Provides
    final OrderDao orderDAO(MongoClient mongoClient, ApplicationConfiguration applicationConfiguration) {
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClient.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        MongoDatabase mongoDatabase = mongoClient.getDatabase(applicationConfiguration.getMongoDBConnection().getDatabase()).withCodecRegistry(pojoCodecRegistry);

        String collectionName = applicationConfiguration.getMongoDBConnection().getCollection();
        if (!collectionExists(mongoDatabase, collectionName)) {
            mongoDatabase.createCollection(collectionName);
        }
        return new OrderDao(mongoDatabase.getCollection(collectionName, Order.class));
    }


    private boolean collectionExists(MongoDatabase mongoDatabase, String collectionName) {
        MongoIterable<String> collectionIterable =  mongoDatabase.listCollectionNames();
        for(String s : collectionIterable) {
            if(s.equals(collectionName)) {
                return true;
            }
        }
        return false;
    }

}
