FROM openjdk:8
WORKDIR /
ADD build/libs/checkout.jar checkout.jar
ADD src/main/resources/configs/application-docker.yml application.yml
CMD java -mx1024m -Xms1024m -jar checkout.jar server application.yml