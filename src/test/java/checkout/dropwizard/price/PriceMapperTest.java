package checkout.dropwizard.price;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.skife.jdbi.v2.StatementContext;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

public class PriceMapperTest {

    @Mock
    ResultSet resultSet;

    @Mock
    StatementContext statementContext;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void priceMapperMapsToPrice() throws SQLException {
        PriceMapper priceMapper = new PriceMapper();

        when(resultSet.getInt("item_number")).thenReturn(1);
        when(resultSet.getBigDecimal("price")).thenReturn(BigDecimal.valueOf(2));
        when(resultSet.getInt("promo_units")).thenReturn(3);
        when(resultSet.getBigDecimal("promo_price")).thenReturn(BigDecimal.valueOf(4));
        Price price = priceMapper.map(1, resultSet, statementContext);
        assertThat(price.getItemNumber(), Matchers.is(1));
        assertThat(price.getPrice(), Matchers.is(BigDecimal.valueOf(2)));
        assertThat(price.getPromoUnits(), Matchers.is(3));
        assertThat(price.getPromoPrice(), Matchers.is(BigDecimal.valueOf(4)));
    }
}
