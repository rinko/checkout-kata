DROP TABLE IF EXISTS PRICE CASCADE;

CREATE TABLE PRICE
(
    item_number int,
    price DECIMAL,
    promo_units int,
    promo_price DECIMAL,

    CONSTRAINT prim_key PRIMARY KEY (item_number)
);

GRANT SELECT, UPDATE, INSERT, TRUNCATE, DELETE ON TABLE PRICE TO postgres_user;
